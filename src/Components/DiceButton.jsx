import React, {useContext, useRef, useState} from 'react';
import {useTranslation} from "react-i18next";
import {StoreContext} from "../utils/store";
import Dice from "./Dice";
import diceRollSound from "./roll_dice.mp3";
import useSound from 'use-sound';
import DiceFace from "./DiceFace";
import {getHighestRoll} from "../utils/countDuplicates";
import {hasGear} from "../utils/hasGear";

const DiceButton = () => {
  const {
    character,
    gearList,
    dicesCount,
    activeStrengthPoints
  } = useContext(StoreContext);

  const diceContainer = useRef(null);
  const { t } = useTranslation();
  const [showDiceWindow, setShowDiceWindow] = useState(false);
  const [showDiceCount, setShowDiceCount] = useState(false);
  const [rolledDiceType, setRolledDiceType] = useState(null);
  const [dices, setDices] = useState(null);
  const [diceRolls, setDiceRolls] = useState([]);

  //mage specific
  const [currentDiceFaces, setCurrentDiceFaces] = useState([]);

  //archer specific
  const [archerDices, setArcherDices] = useState(0);

  const highestRoll = getHighestRoll(gearList, diceRolls);
  const showHelmIcon = highestRoll !== Math.max(...diceRolls);
  const [audioPlay] = useSound(diceRollSound, {
    interrupt: true,
  });

  const toggleDiceWindow = () => {
    setShowDiceWindow(!showDiceWindow);
    setArcherDices(dicesCount);
    setDiceRolls([]);
    setRolledDiceType(null);
  }

  const rollDice = (diceCount) => {
    audioPlay();
    toggleDiceWindow();
    setRolledDiceType('basic');
    setShowDiceCount(true);
    const diceFaces = [1, 2, 3, 4, 5, 6];
    setCurrentDiceFaces(diceFaces);

    const newRolls = [];
    let callbackCount = 0;
    const callback = (roll) => {
      newRolls.push(roll);
      callbackCount++;
      if(callbackCount === dicesCount){
        setDiceRolls(newRolls)
      }
    }

    const dices = [];
    for (let step = 0; step < diceCount; step++) {
      dices.push(
        <Dice key={`defaultdice-${step}`} values={diceFaces} callback={callback} />
      )
    }

    setDices(dices);
  }

  const rollDiceForArcher = () => {
    setRolledDiceType('ranged');
    setShowDiceWindow(false);
    setShowDiceCount(true);
    setDiceRolls([]);
    setArcherDices(archerDices-1);
    audioPlay();

    const callback = (roll) => {
      setDiceRolls([roll])
    }

    setDices(
      <Dice type={"ranged"} values={[1, 2, 3, 4, 5, 6]} callback={callback} />
    );
  }

  const rollWhiteDice = () => {
    audioPlay();
    toggleDiceWindow();
    setRolledDiceType('white');
    setShowDiceCount(true);
    const diceFaces = [4, 5, 5, 6, 6, 7];
    setCurrentDiceFaces(diceFaces);
    const callback = (roll) => {
      setDiceRolls([roll])
    }

    setDices(
      <Dice type={"white"} values={diceFaces} callback={callback} />
    );
  };

  const rollBlackDice = () => {
    audioPlay();
    toggleDiceWindow();
    setRolledDiceType('black');
    setShowDiceCount(true);
    const diceFaces = [6, 6, 8, 10, 10, 12];
    setCurrentDiceFaces(diceFaces);
    const callback = (roll) => {
      setDiceRolls([roll])
    }

    setDices(
      <Dice type={"black"} values={diceFaces} callback={callback} />
    );
  };

  const showOppositeSide = () => {
    const oppositeDiceFaces = [...currentDiceFaces].reverse();
    const oppositeRollIndex = [...currentDiceFaces].reverse().indexOf(highestRoll);

    setCurrentDiceFaces(oppositeDiceFaces);
    setDiceRolls([currentDiceFaces[oppositeRollIndex]]);

    setDices(
      <DiceFace type={rolledDiceType || 'default'} values={currentDiceFaces} currentFaceIndex={oppositeRollIndex} />
    );
  };

  const rollHornDice = () => {
    const maxCharacterDices = Math.max(...character.dices);
    rollDice(maxCharacterDices);
  };

  const rollHornDiceForArcher = () => {
    const maxCharacterDices = Math.max(...character.dices);

    setRolledDiceType('ranged');
    setShowDiceWindow(false);
    setShowDiceCount(true);
    setDiceRolls([]);
    setArcherDices(maxCharacterDices-1);
    audioPlay();

    const callback = (roll) => {
      setDiceRolls([roll])
    }

    setDices(
      <Dice type={"ranged"} values={[1, 2, 3, 4, 5, 6]} callback={callback} />
    );
  };

  return(
    <div className={"dice-menu"}>
      {!showDiceCount && (
      <button className="btn toggle-dice-menu-btn" onClick={() => toggleDiceWindow()}><i className="fa fa-dice"/></button>
      )}
      {showDiceWindow && (
        <div className={"dices"}>
          <div>
            {character.hasBasicDiceAttack && (
              <button className="dice-roll-option-btn" onClick={() => rollDice(dicesCount)}>{t('dice.attack_default')}</button>
            )}
            {(character.type === "archer" || character.type === "renegade_keeper" || hasGear(gearList, "bow")) && (
              <button className="dice-roll-option-btn" onClick={() => rollDiceForArcher()}>{t('dice.attack_ranged')}</button>
            )}
            {(hasGear(gearList, "runestone_green") && hasGear(gearList, "runestone_blue") && hasGear(gearList, "runestone_yellow")) && (
            <button className="dice-roll-option-btn" onClick={() => rollBlackDice(dicesCount)}>{t('dice.attack_runestone')}</button>
            )}
            <span className={"special-dicerolls"}>{t('dice.special_rolls')}</span>
            <button className="dice-roll-option-btn" onClick={() => rollWhiteDice(dicesCount)}>{t('dice.attack_water_spirit')}</button>
            {character.hasBasicDiceAttack && (
              <button className="dice-roll-option-btn" onClick={() => rollHornDice()}>{t('dice.attack_horn')}</button>
            )}
            {(character.type === "archer" || character.type === "renegade_keeper" || hasGear(gearList, "bow")) && (
              <button className="dice-roll-option-btn" onClick={() => rollHornDiceForArcher()}>{t('dice.attack_horn_ranged')}</button>
            )}
          </div>
        </div>
      )}
      {showDiceCount && (
        <div className="dice-box" ref={diceContainer}>
          {isFinite(highestRoll) && (
            <div className={"total-rolled-count"}>
              {t('dice.rolled')}: <i className={`fas ${showHelmIcon ? 'fa-hard-hat' : 'fa-dice-d6'}`}/> {highestRoll} + <i className="fas fa-fist-raised"/> {activeStrengthPoints} = {highestRoll+activeStrengthPoints}
            </div>
          )}
          <div className={"die-container"}>
            {dices != null && dices}
          </div>
          <button className="btn toggle-dice-menu-btn" onClick={() => setShowDiceCount(false)}><i className="fa fa-times"/></button>
          {(character.type === "mage") && (
            <button className="btn dice-roll-option-btn" onClick={() => showOppositeSide()}>Other side</button>
          )}
          {(rolledDiceType === 'ranged' && archerDices > 0) && (
            <button className="btn dice-roll-option-btn" onClick={() => rollDiceForArcher()}>{`Rolls(${archerDices} left)`}</button>
          )}
        </div>
      )}
    </div>
)};

export default DiceButton;
