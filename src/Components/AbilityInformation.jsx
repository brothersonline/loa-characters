import React, {useContext} from 'react';
import {useTranslation} from "react-i18next";
import {StoreContext} from "../utils/store";

const AbilityInformation = () => {
  const { character } = useContext(StoreContext);
  const { t } = useTranslation();

  return(
    <section className="text-center">
      <div className="container ability">
        <div className="row text-center animated fadeInUp">
          <div className="col-md-12 wp4">
            <h2>{t('ability')}: </h2>
            <div dangerouslySetInnerHTML={{ __html: t(`characters.${character.id}.ability`) }} />
          </div>
        </div>
      </div>
    </section>
  );
}

export default AbilityInformation;
