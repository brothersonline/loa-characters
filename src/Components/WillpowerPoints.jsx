import React, {useContext} from 'react';
import {useTranslation} from "react-i18next";
import {getWillpowerPoints} from "../Functions/points";
import {StoreContext} from "../utils/store";

const WillpowerPoints = () => {
  const {
    character,
    toggleWillpowerPoints,
    activeWillpowerPoints,
    dicesCount
  } = useContext(StoreContext)
  const { t } = useTranslation();

  const willpowerPoints = getWillpowerPoints();

  const dices = [];
  for (let i = 0; i < dicesCount; i++) {
    dices.push(i);
  }

  return (
    <div className="row text-center animated fadeInUp">
      <div className="col-md-12 wp4">
        <h2><div className={`willpower-icon`}/> {t('willpower_points')}</h2>
        <span>{t('current_number_of_dices')}:</span>
        <div>{dices.map((dice) => (
          <i key={dice} className="fa fa-dice-d6 dice"/>
        ))}
        </div>
        <div className="willpower-points">
          {willpowerPoints.map((point) => (
            <button
              key={point}
              className={`btn willpower_point_${point} ${activeWillpowerPoints === point ? "" : "inactive-filter-btn"}`}
              onClick={() => toggleWillpowerPoints(point, character)}>{point}</button>
          ))}
        </div>
      </div>
    </div>
)};

export default WillpowerPoints;
