import React, {useEffect, useRef} from 'react';
import './Dice.css'

const sides = [
  "translateZ(-50px) rotateY(0deg)",
  "translateZ(-50px) rotateX(-180deg)",
  "translateZ(-50px) rotateY(-90deg)",
  "translateZ(-50px) rotateY(90deg)",
  "translateZ(-50px) rotateX(-90deg)",
  "translateZ(-50px) rotateX(90deg)"
];

const Dice = ({type = 'default', values, callback}) => {
  const diceElement = useRef(null);

  let divs = sides.map((side, index) => {
    return <div key={`dice-side-${index}`} className="side">{values ? values[index] : index+1}</div>
  });

  useEffect(() => {
    const roll = () => {
      const dice = diceElement.current;
      dice.classList.add("rolling");
      setTimeout(function () {
        const roll = Math.floor(Math.random() * (sides.length))
        callback(values[roll]);
        dice.classList.remove("rolling");
        dice.style.transform = sides[roll];
      }, 750);
    };
    roll();
  }, [values, callback]);

  return(
    <div className="dice-container">
      <div ref={diceElement} id="dice" className={'d' + sides.length + ` ${type}`}>
        { divs }
      </div>
    </div>
  );
}

export default Dice;
