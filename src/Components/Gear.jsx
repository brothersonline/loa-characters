import React, { useContext } from 'react';
import { useTranslation } from 'react-i18next';
import './Gear.css';
import { StoreContext } from '../utils/store';

function Gear() {
  const { gearList, setGearList, setCurrentView } = useContext(StoreContext);
  const { t } = useTranslation();

  const handleInputChange = (e, index) => {
    const { name, value } = e.target;
    const list = [...gearList];
    list[index][name] = value;
    setGearList(list);
  };

  const handleRemoveClick = (index) => {
    const list = [...gearList];
    list.splice(index, 1);
    setGearList(list);
  };

  const handleAddClick = () => {
    setGearList([...gearList, { value: '' }]);
  };

  const decreaseItemUses = (index, item) => {
    const list = [...gearList];
    // eslint-disable-next-line no-param-reassign
    item.uses -= 1;

    if (item.uses === 0) {
      list.splice(index, 1);
    }

    setGearList(list);
  };

  return (
    <section className="gear text-center">
      <div className="container ability">
        <div className="row text-center animated fadeInUp">
          <div className="col-md-12 wp4">
            <h2>{t('gear')}</h2>
            {gearList.map((x, i) => {
              if (gearList[i].isSupportedItem) {
                const item = gearList[i].value;
                const itemImagePath = `${process.env.PUBLIC_URL}/img/items/${item.id}.png`;
                const style = {
                  backgroundImage: `url(${itemImagePath})`,
                  backgroundRepeat: 'no-repeat',
                  backgroundPosition: 'center center',
                  backgroundSize: 'contain',
                };

                return (
                  // eslint-disable-next-line react/no-array-index-key
                  <div key={`gearItem${i}`} className="supported-item">
                    <div className={"gear-value"}>
                      <div className="supported-item-image" style={style} />
                      <div className="supported-item-details">
                        <h1>{t(`items.${item.id}.title`)}</h1>
                        <div dangerouslySetInnerHTML={{ __html: t(`items.${item.id}.description`) }} />
                        {item.uses && (
                          <div>
                            <p dangerouslySetInnerHTML={{ __html: t('uses_left', { item: item.uses }) }} />
                            <button className="supported-item-use-button" onClick={() => decreaseItemUses(i, item)} type="button">Gebruiken</button>
                          </div>
                        )}
                      </div>
                    </div>
                    <button className="btn gear-delete-button" onClick={() => handleRemoveClick(i)} type="button">-</button>
                  </div>
                );
              }
              return (
                // eslint-disable-next-line react/no-array-index-key
                <div key={`gearItem${i}`} className="gear-item">
                  <input
                    name="value"
                    placeholder={t('name_item')}
                    className="gear-value"
                    value={x.value}
                    onChange={(e) => handleInputChange(e, i)}
                  />
                  <button className="btn gear-delete-button" onClick={() => handleRemoveClick(i)} type="button">-</button>
                </div>
              );
            })}
          </div>
          {gearList.length < 5 && (
          <div>
            <button className="btn" onClick={handleAddClick} type="button">+</button>
            <button className="btn" onClick={() => setCurrentView('item-selector')} type="button"><i className="fas fa-search" /></button>
          </div>
          )}
        </div>
      </div>
    </section>
  );
}

export default Gear;
