import React, {useEffect, useRef} from 'react';
import './Dice.css'

const sides = [
  "translateZ(-100px) rotateY(0deg)",
  "translateZ(-100px) rotateX(-180deg)",
  "translateZ(-100px) rotateY(-90deg)",
  "translateZ(-100px) rotateY(90deg)",
  "translateZ(-100px) rotateX(-90deg)",
  "translateZ(-100px) rotateX(90deg)"
];

const DiceFace = ({type= 'default', values, currentFaceIndex}) => {
  const diceElement = useRef(null);
  let divs = sides.map((side, index) => {
    return <div key={`dice-side-${index}`} className="side">{values ? values[index] : index+1}</div>
  });

  useEffect(() => {
    const showFace = () => {
      const dice =  diceElement.current;
      dice.style.transform = sides[currentFaceIndex];
    };
    showFace();
  }, [values, currentFaceIndex]);

  return(
    <div className="dice-container">
      <div ref={diceElement} id="dice" className={'d' + sides.length + ` ${type}`}>
        { divs }
      </div>
    </div>
  );
}

export default DiceFace;
