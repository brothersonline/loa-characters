import React, {useContext, useState} from 'react';
import {useTranslation} from "react-i18next";
import guides from "../../guides.json";
import "./HelpView.css";
import {StoreContext} from "../../utils/store";

function HelpView() {
  const {setCurrentView} = useContext(StoreContext);
  const [currentGuide, setCurrentGuide] = useState(null);
  const { t } = useTranslation();

  if(currentGuide){
    return (
      <div className={"help-view"}>
        <div className={"top-btns"}>
          <button className="btn" onClick={() => setCurrentGuide(null)}><i className={`fas fa-arrow-left`}/></button>
        </div>
        <section className="text-center">
          <h1>{t('help_database')}</h1>
        </section>
        <section className="points text-center">
          <div className="container">
            <div className={"current-guide"} key={currentGuide.id}>
              <h3>{t(`guides.${currentGuide.id}.title`)}</h3>
              <div dangerouslySetInnerHTML={{ __html: t(`guides.${currentGuide.id}.description`) }} />
            </div>
          </div>
        </section>
      </div>
    )
  }

  return (
    <div className={"help-view"}>
      <div className={"top-btns"}>
        <button className="btn" onClick={() => setCurrentView('default')}><i className={`fas fa-user`}/></button>
      </div>
      <section className="text-center">
        <h1>{t('help_database')}</h1>
      </section>
      <section className="points text-center">
        <div className="container">
          <div className="row text-center animated fadeInUp">
            <div className="col-md-12 wp4">
              {guides.map((guide) => {
                return (
                  <div className={"guide-row"} key={guide.id} onClick={()=> setCurrentGuide(guide)}>
                    <h3>{t(`guides.${guide.id}.title`)}</h3>
                  </div>
                )
              })}
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}

export default HelpView;
