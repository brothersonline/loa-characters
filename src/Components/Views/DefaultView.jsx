import React, { useContext, useState } from 'react';
import Gear from '../Gear';
import HeroDetails from '../HeroDetails';
import AbilityInformation from '../AbilityInformation';
import CoinsPurse from '../CoinsPurse';
import StrengthPoints from '../StrengthPoints';
import DiceButton from '../DiceButton';
import WillpowerPoints from '../WillpowerPoints';
import HelpView from './HelpView';
import ItemSelectorView from './ItemSelectorView';
import { StoreContext } from '../../utils/store';

function DefaultView() {
  const {
    character,
    setCharacter,
    currentView,
    setCurrentView,
    saveProgress,
  } = useContext(StoreContext);
  const [showSaveResultMessage, setShowSaveResultMessage] = useState(false);

  const imagePath = `${process.env.PUBLIC_URL}/img/characters/${character.id}.jpg`;
  const style = {
    background: `url(${imagePath}) no-repeat bottom center`,
    backgroundPosition: 'top center',
    backgroundSize: 'contain',
    backgroundAttachment: 'fixed',
  };

  const save = () => {
    saveProgress();
    setShowSaveResultMessage(true);
    setTimeout(() => { setShowSaveResultMessage(false); }, 3000);
  };

  if (currentView === 'help') {
    return (<HelpView />);
  }

  if (currentView === 'item-selector') {
    return (<ItemSelectorView />);
  }

  return (
    <>
      <div className="top-btns">
        <button className="btn" onClick={() => setCharacter(null)} type="button"><i className="fas fa-house-user" /></button>
        <button className="btn" onClick={() => setCurrentView('help')} type="button"><i className="fas fa-question" /></button>
        <button className="btn" onClick={() => save(character)} type="button"><i className="fas fa-save" /></button>
        {showSaveResultMessage && <span className="save-successful">Save successfull</span>}
      </div>
      <DiceButton />
      <header id="home" style={style}>
        <HeroDetails />
      </header>
      <AbilityInformation />
      <Gear />
      <CoinsPurse />
      <section className="points text-center">
        <div className="container">
          <StrengthPoints />
          <WillpowerPoints />
        </div>
      </section>
    </>
  );
}

export default DefaultView;
