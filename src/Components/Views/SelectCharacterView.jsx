import React, {useContext} from 'react';
import {useTranslation} from "react-i18next";
import characters from "../../characters.json";
import "./SelectCharacterView.css";
import SavedCharacter from "../SavedCharacter";
import {StoreContext} from "../../utils/store";

function SelectCharacterView() {
  const {
    setCharacter,
    toggleWillpowerPoints,
    resetProgress,
  } = useContext(StoreContext)

  const { t } = useTranslation();

  const setInfo = (character) => {
    resetProgress();
    setCharacter(character);
    toggleWillpowerPoints(7, character);
    window.scrollTo(0, 0);
  }

  const savedCharacter = JSON.parse(localStorage.getItem('current_loa_character') || null);

  return (
    <>
      <header className="hero-all"/>
      <section className="text-center">
        <h1>{t('choose_your_character')}</h1>
      </section>
      <section className="points text-center">
        <div className="container">
          <div className="row text-center animated fadeInUp">
            {savedCharacter && <SavedCharacter savedCharacter={savedCharacter} />}
            <div className="col-md-12 wp4">
              <h3>{t('new_characters')}</h3>
              {characters.map((character) => {
                const characterImagePath = `${process.env.PUBLIC_URL}/img/characters/${character.id}.jpg`;
                const classIconPath = `${process.env.PUBLIC_URL}/img/classIcons/${character.type}.png`;
                const style = {
                  background: `url(${characterImagePath}) no-repeat`,
                  backgroundPosition: `top center`,
                  backgroundSize: `cover`,
                };

                let highestDiceCount = 0;

                character.dices.forEach((dice) => {
                  if(dice > highestDiceCount){
                    highestDiceCount = dice;
                  }
                })

                return (
                  <div className={"class-row"} onClick={() => setInfo(character)} key={character.id}>
                    <div className={"btn class-row-character-btn"} style={style}/>
                    <div className={"class-row-character-name"}>
                      <h2>{character.name}</h2>
                      <p className={"source"}>{t('source.title')}{t(`source.${character.source}`)}</p>
                      {!character.hasDiceSupport && <p><i className={`fas fa-dice`}/> {t('dice.physical_dices_needed')}</p>}
                    </div>
                    <i className={`fa-class-icon fas fa-dice-d6`}/><span className={"class-row-dice-count"}>{highestDiceCount}</span>
                    <div className={"class-row-divider"} />
                    <i className={`fa-class-icon fas fa-${character.sex === "male" ? "mars": "venus"}`}/>
                    <div className={"class-row-divider"} />
                    <div className={`class-row-class-btn`} style={{
                      background: `url(${classIconPath}) no-repeat center center`,
                      backgroundSize: `contain`,
                    }}/>
                  </div>
                )
              })}
            </div>
          </div>
        </div>
      </section>
    </>
  );
}

export default SelectCharacterView;
