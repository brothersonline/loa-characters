import React, { useContext } from 'react';
import { useTranslation } from 'react-i18next';
import items from '../../items.json';
import './ItemSelectorView.css';
import { StoreContext } from '../../utils/store';

function ItemSelectorView() {
  const { setCurrentView, gearList, setGearList } = useContext(StoreContext);
  const { t } = useTranslation();

  const addToGear = (item) => {
    setGearList([...gearList, { value: { ...item }, isSupportedItem: true }]);
    setCurrentView('default');
  };

  return (
    <div className="item-selector-view">
      <div className="top-btns">
        <button className="btn" onClick={() => setCurrentView('default')} type="button"><i className="fas fa-user" /></button>
      </div>
      <section className="text-center">
        <h1>{t('items_title')}</h1>
      </section>
      <section className="points text-center">
        <div className="container">
          <div className="row text-center animated fadeInUp">
            <div className="col-md-12 wp4">
              {items.map((item) => {
                const itemImagePath = `${process.env.PUBLIC_URL}/img/items/${item.id}.png`;
                const style = {
                  backgroundImage: `url(${itemImagePath})`,
                  backgroundRepeat: 'no-repeat',
                  backgroundPosition: 'center center',
                  backgroundSize: 'contain',
                };

                return (
                  <div className="item-row supported-item" key={item.id} onClick={() => addToGear(item)}>
                    <div className={"gear-value"}>
                    <div className="supported-item-image" style={style} />
                    <div className="supported-item-details">
                      <h1>{t(`items.${item.id}.title`)}</h1>
                      <div dangerouslySetInnerHTML={{ __html: t(`items.${item.id}.description`) }} />
                    </div>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}

export default ItemSelectorView;
