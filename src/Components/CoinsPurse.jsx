import React, {useContext} from 'react';
import {useTranslation} from "react-i18next";
import {StoreContext} from "../utils/store";

const CoinsPurse = () => {
  const {coinsAmount, setCoinsAmount } = useContext(StoreContext);
  const { t } = useTranslation();

  function increaseCoins() {
    setCoinsAmount(coinsAmount+1);
  }

  function decreaseCoins() {
    if(coinsAmount === 0){
      return;
    }
    setCoinsAmount(coinsAmount-1);
  }

  return (
    <section className="coins text-center">
      <div className="container ability">
        <div className="row text-center animated fadeInUp">
          <div className="col-md-12 wp4">
            <h2><i className="fas fa-coins"/> {t('gold')}: {coinsAmount}</h2>
            {coinsAmount !== 0 && (<button
              className={`btn coins-btn`}
              onClick={() => decreaseCoins()}>-
            </button>)}
            <button
              className={`btn coins-btn`}
              onClick={() => increaseCoins()}>+
            </button>
          </div>
        </div>
      </div>
    </section>
  )};

export default CoinsPurse;
