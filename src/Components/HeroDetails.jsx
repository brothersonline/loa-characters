import React, {useContext} from 'react';
import {useTranslation} from "react-i18next";
import {StoreContext} from "../utils/store";

const HeroDetails = () => {
  const { character } = useContext(StoreContext);
  const { t } = useTranslation();
  const name = character.name;
  const title = t(`characters.${character.id}.title`);
  const rank = character.rank;

  return(
    <section className="hero">
      <div className="hero-text">
        <div className="container">
          <div className="row text-center animated fadeInDown">
            <div className="col-md-12 wp4">
              <h1 className="hero-title">{name}</h1>
              <h3 className="hero-subtitle">{title}</h3>
              <h3 className="hero-rank">{t('rank')} {rank}</h3>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
};

export default HeroDetails;
