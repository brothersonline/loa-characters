import React, {useContext} from 'react';
import {useTranslation} from "react-i18next";
import {StoreContext} from "../utils/store";

const SavedCharacter = ({savedCharacter}) => {
  const {resetProgress, setCharacter, setActiveWillpowerPoints, setActiveStrengthPoints, setGearList, setCoinsAmount, toggleWillpowerPoints} = useContext(StoreContext)
  const { t } = useTranslation();

  const setInfoFromSavedCharacter = (character) => {
    resetProgress();
    setCharacter(character);
    setActiveWillpowerPoints(character.savedWillpowerPoints);
    setActiveStrengthPoints(character.savedStrengthPoints);
    setGearList(character.savedGearList);
    setCoinsAmount(character.savedCoinsAmount);
    toggleWillpowerPoints(character.savedWillpowerPoints, character);

    window.scrollTo(0, 0);
  }

  const characterImagePath = `${process.env.PUBLIC_URL}/img/characters/${savedCharacter.id}.jpg`;
  const style = {
    background: `url(${characterImagePath}) no-repeat`,
    backgroundPosition: `top center`,
    backgroundSize: `cover`,
  };

  return (
    <div className="col-md-12 wp4">
      <h3>{t('saved_character')}</h3>
      <div className={"class-row"} onClick={() => setInfoFromSavedCharacter(savedCharacter)}>
        <div className={"btn class-row-character-btn"} style={style}/>
        <div className={"class-row-character-name"}>
          <h2>{savedCharacter.name}</h2>
          {!savedCharacter.hasDiceSupport && <p>({t('dice.physical_dices_needed')})</p>}
        </div>
        <div className="date-saved">{savedCharacter.dateSaved}</div>
      </div>
    </div>
  )};

export default SavedCharacter;
