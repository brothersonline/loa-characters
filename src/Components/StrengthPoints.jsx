import React, {useContext} from 'react';
import {getStrengthPoints} from "../Functions/points";
import {useTranslation} from "react-i18next";
import {StoreContext} from "../utils/store";

const StrengthPoints = () => {
  const {
    activeStrengthPoints,
    setActiveStrengthPoints
  } = useContext(StoreContext);

  const { t } = useTranslation();

  function toggleStrengthPoints(point){
    setActiveStrengthPoints(point);
  }

  return (
  <div className="row text-center animated fadeInUp">
    <div className="col-md-12 wp4">
      <h2><i className="fas fa-fist-raised"/> {t('strength_points')}</h2>
      {getStrengthPoints().map((point) => (
        <button
          key={point}
          className={`btn strength_point_${point} ${activeStrengthPoints === point ? "active-strength-point" : "inactive-filter-btn"}`}
          onClick={() => toggleStrengthPoints(point)}>{point}</button>
      ))}
    </div>
  </div>
)};

export default StrengthPoints;
