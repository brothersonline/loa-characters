import React, {Suspense, useContext } from "react";
import DefaultView from "./Components/Views/DefaultView";
import SelectCharacterView from "./Components/Views/SelectCharacterView";
import './Components/i18n';
import {StoreContext} from "./utils/store";

function App() {
  const { character } = useContext(StoreContext)
  const specialCharacters = [];

  return (
    <div className="App">
      <Suspense fallback={<div>Loading...</div>}>
        {!character ? (
          <SelectCharacterView />
        ) : specialCharacters.includes(character.type)
          ? null
          : <DefaultView />}
      </Suspense>
    </div>
  );
}

export default App;
