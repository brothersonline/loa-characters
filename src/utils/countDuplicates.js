import {hasGear} from "./hasGear";

const countDuplicates = names =>
  names.reduce((a, b) => ({ ...a,
    [b]: (a[b] || 0) + 1
  }), {})

export function getHighestRoll(gearList = [], diceRolls = []) {
  if(hasGear(gearList, "helmet")){
    const duplicateRolls = countDuplicates(diceRolls);
    const totals = Object.keys(duplicateRolls).map(k => k *  duplicateRolls[k]);

    if(Object.keys(duplicateRolls).length !== 0){
      return Math.max(...totals);
    }
  }
  return Math.max(...diceRolls);
}
