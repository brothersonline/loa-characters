import React, { useState } from 'react';

export const StoreContext = React.createContext(null);

export default ({ children }) => {
  const [character, setCharacter] = useState(null);
  const [activeWillpowerPoints, setActiveWillpowerPoints] = useState(7);
  const [activeStrengthPoints, setActiveStrengthPoints] = useState(1);
  const [gearList, setGearList] = useState([]);
  const [coinsAmount, setCoinsAmount] = useState(0);
  const [dicesCount, setActiveDicesCount] = useState(0);
  const [currentView, setCurrentView] = useState('default');

  const resetProgress = () => {
    setCharacter(null);
    setActiveWillpowerPoints(7);
    setActiveStrengthPoints(1);
    setGearList([]);
    setCoinsAmount(0);
    setActiveDicesCount(0);
    setCurrentView('default');
  };

  const saveProgress = () => {
    const d = new Date();
    character.dateSaved = `${[d.getMonth() + 1,
      d.getDate(),
      d.getFullYear()].join('/')} ${
      [d.getHours(),
        d.getMinutes(),
        d.getSeconds()].join(':')}`;

    character.savedWillpowerPoints = activeWillpowerPoints;
    character.savedStrengthPoints = activeStrengthPoints;
    character.savedCoinsAmount = coinsAmount;
    character.savedGearList = gearList;

    localStorage.setItem('current_loa_character', JSON.stringify(character));
  };

  const toggleWillpowerPoints = (willpowerPoints, newCharacter) => {
    const toggleWillpowerPointsCentaur = (point) => {
      setActiveWillpowerPoints(point);
      if (point > 14) {
        setActiveDicesCount(newCharacter.dices[3]);
        return;
      }
      if (point > 9) {
        setActiveDicesCount(newCharacter.dices[2]);
        return;
      }
      if (point > 4) {
        setActiveDicesCount(newCharacter.dices[1]);
        return;
      }
      setActiveDicesCount(newCharacter.dices[0]);
    };

    const toggleWillpowerPointsForOthers = (point) => {
      setActiveWillpowerPoints(point);

      if (point > 13) {
        setActiveDicesCount(newCharacter.dices[2]);
        return;
      }
      if (point > 6) {
        setActiveDicesCount(newCharacter.dices[1]);
        return;
      }
      setActiveDicesCount(newCharacter.dices[0]);
    };

    if (newCharacter.type === 'centaur') {
      toggleWillpowerPointsCentaur(willpowerPoints);
    } else {
      toggleWillpowerPointsForOthers(willpowerPoints);
    }
  };

  // useEffect(() => {
  //   console.log('bla!');
  //   // Your code here
  // }, []);

  const store = {
    character,
    activeWillpowerPoints,
    setActiveWillpowerPoints,
    activeStrengthPoints,
    setActiveStrengthPoints,
    gearList,
    setGearList,
    coinsAmount,
    setCoinsAmount,
    dicesCount,
    setActiveDicesCount,
    currentView,
    setCurrentView,
    setCharacter,
    toggleWillpowerPoints,
    resetProgress,
    saveProgress,
  };

  // eslint-disable-next-line react/jsx-filename-extension
  return <StoreContext.Provider value={store}>{children}</StoreContext.Provider>;
};
